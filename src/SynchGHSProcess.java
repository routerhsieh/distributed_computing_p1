import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;

public class SynchGHSProcess extends Thread{
	private char myID;//ID of process
	private SynchGHSProcess parent;//parent
	private HashSet<SynchGHSProcess> child;//hashset of child process
	private double[] adjList;//adjacent list of neighboring process
	private int level;//level of process
	private int compID;//component ID
	private SynchGHSProcess leader;
	private boolean amILeader,flag;//flag: for forming links in merging
	private int index;//index of process
	private HashSet<ProcessMessage> msgFromParent,msgFromChild;
	private HashSet<CompMessage>msgFromOtherComp;
	private int compToJoin;
	private ArrayBlockingQueue<MasterMessage> msgFromMaster;
	

	public SynchGHSProcess(char myID,double[] adjList,int compID) {
		this.myID = myID;//process ID
		this.parent=null;//parent of this process in MST
		this.child=new HashSet<>();//hashset of child of this process in MST
		this.adjList = adjList;//adjacency list of process
		this.level=0;//initially all the process will in level 0
		this.compID=compID;
		this.leader=this;//initially all process will be its leader
		this.amILeader=true;//every process will be leader initially
		this.flag=false;
		this.index=compID;//index of process
		this.msgFromParent=new HashSet<>();
		this.msgFromChild=new HashSet<>();
		this.msgFromOtherComp=new HashSet<>();
		this.msgFromMaster=new ArrayBlockingQueue<>(2);
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public ArrayBlockingQueue<MasterMessage> getMsgFromMaster() {
		return msgFromMaster;
	}

	public int getCompToJoin() {
		return compToJoin;
	}

	public void setCompToJoin(int compToJoin) {
		this.compToJoin = compToJoin;
	}

	public HashSet<CompMessage> getMsgFromOtherComp() {
		return msgFromOtherComp;
	}

	public HashSet<ProcessMessage> getMsgFromParent() {
		return msgFromParent;
	}

	public HashSet<ProcessMessage> getMsgFromChild() {
		return msgFromChild;
	}

	public void setMsgFromChild(HashSet<ProcessMessage> msgFromChild) {
		this.msgFromChild = msgFromChild;
	}

	public SynchGHSProcess getLeader() {
		return leader;
	}

	public void setLeader(SynchGHSProcess leader) {
		this.leader = leader;
	}

	public boolean isAmILeader() {
		return amILeader;
	}

	public void setAmILeader(boolean amILeader) {
		this.amILeader = amILeader;
	}

	public char getMyID() {
		return myID;
	}

	public SynchGHSProcess getParent() {
		return parent;
	}

	public void setParent(SynchGHSProcess parent) {
		this.parent = parent;
	}

	public HashSet<SynchGHSProcess> getChild() {
		return child;
	}

	public void addChild(SynchGHSProcess child) {
		this.child.add(child);
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getCompID() {
		return compID;
	}

	public void setCompID(int compID) {
		this.compID = compID;
	}

	public void run() {
		try {
			synchronized (this) {
				while(true){
					MasterMessage tempMsg=this.getMsgFromMaster().take();
					String[] token=tempMsg.getMsg().split("#");
					if(token[0].toUpperCase().equals("DONE"))//break if done message received from Master
						break;
					else if(token[0].equals("Start phase")){
						convergeCast();//convergeCast finished. Replying back to Master
						Master.msgFromComp.put(new MasterMessage(this.getMyID()+"#round"+token[1]+"#finished"));						
					}			
					else{ /*(token[0].equals("Start round"))*/
						merge();
						this.setFlag(false);
						Master.msgFromComp.put(new MasterMessage(this.getMyID()+"#round"+token[1]+"#finished"));
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error in SynchGHSProcess.run(): "+e.getMessage());
			e.getStackTrace();
		}
	}

	public void merge(){		
		if(this.isAmILeader()==true){
			/*leader of respective component does the merge*/
			Iterator<CompMessage> iterator=this.getMsgFromOtherComp().iterator();
			HashMap<Integer, String> request=new HashMap<>();
			while(iterator.hasNext()){
				CompMessage temp=iterator.next();
				request.put(temp.from, temp.msg);
			}
			if(request.containsKey(this.getCompToJoin())){
				/*both components want to join to each other. No other dependency*/
				String value=request.get(this.getCompToJoin());
				String[] token=value.split("#");
				SynchGHSProcess tempFrom=getProcess((int)(token[2].charAt(0)-65));
				SynchGHSProcess tempTo=getProcess((int)(token[1].charAt(0)-65));
				modifyLinks(tempFrom);//make tempFrom process the new root 'locally'
				tempFrom.setCompToJoin(-1);//merged
				if(tempFrom.getMyID()<tempTo.getMyID()){
					tempFrom.setParent(tempTo);//setting parent
					tempFrom.setAmILeader(false);
				}
				else{
					tempFrom.setCompToJoin(-1);//merged
					this.setAmILeader(false);
					tempFrom.setAmILeader(true);//new leader of merged components
					tempFrom.addChild(tempTo);//add as a child
					HashSet<SynchGHSProcess> list=findComponentProcess(tempTo.getCompID());
					Iterator<SynchGHSProcess> iterator2=list.iterator();
					while(iterator2.hasNext()){
						SynchGHSProcess tempProcess=iterator2.next();
						tempProcess.setCompID(tempFrom.getCompID());
						tempProcess.setLeader(tempFrom);
						tempProcess.setLevel(Master.phase+1);
					}
					HashSet<SynchGHSProcess> list2=findComponentProcess(tempFrom.getCompID());
					iterator2=list2.iterator();
					while(iterator2.hasNext()){
						SynchGHSProcess tempProcess=iterator2.next();
						tempProcess.setLeader(tempFrom);//set new leader of all process in my component
						tempProcess.setLevel(Master.phase+1);
					}
				}
			}
			else{
				HashSet<SynchGHSProcess> list=findComponentProcess(this.getCompID());
				HashSet<CompMessage> tempCompMsg=new HashSet<>();
				System.out.println(this.getMyID()+": Merge 2");
				
			}
		}
	}
	
	/*for modifying parent child links of component before merging*/
	public void modifyLinks(SynchGHSProcess process){
		process.getChild().clear();//clear the previous child of this process
		LinkedList<SynchGHSProcess> queue=new LinkedList<>();
		queue.add(process);
		while(queue.isEmpty()){
			SynchGHSProcess temp=queue.remove();
			temp.setFlag(true);//visited
			for(int i=0;i<temp.adjList.length;i++){
				SynchGHSProcess child=this.getProcess(i);
				if(temp.adjList[i]>0 && child.getCompID()==process.getCompID() && child.isFlag()==false){//check only for process in same component
					child.setParent(temp);
					temp.addChild(child);
					queue.add(child);
				}
			}
		}
	}

	/*For finding MWOE*/
	void convergeCast() {
		if(isAmILeader()){
			int msgSent=0;
			Iterator<SynchGHSProcess> iterator=this.getChild().iterator();
			while(iterator.hasNext()){
				SynchGHSProcess temp=iterator.next();
				temp.addMsgFromParent(new ProcessMessage(this, temp, "Test"));
				msgSent++;
			}
			while(this.getMsgFromChild().size()!=msgSent){
				//wait until acknowledgement of all the sent message have been received
			}
			ProcessMessage temp=selectMessageWithMinWt(this.getMsgFromChild(),this.adjList);
			String[] token=temp.msg.split("#");
			//System.out.println(this.getMyID()+"("+this.getCompID()+")"+"Comp: "+token[1]+" messgage: "+temp);
			SynchGHSProcess tempToLeader=temp.to.getLeader();
			tempToLeader.addMsgFromOtherComp(new CompMessage(this.getCompID(), tempToLeader.getCompID(), token[0]+"#"+temp.from.getMyID()+"#"+temp.to.getMyID()));
			this.setCompToJoin(tempToLeader.getCompID());//setting the value of other component to join	
		}// I'm the Leader
		else {
			while (this.msgFromParent.size() == 0) {}
			int msgSent = 0;
			if (this.child.size() > 0) {
				Iterator<SynchGHSProcess> iterator=this.getChild().iterator();
				while(iterator.hasNext()){
					SynchGHSProcess temp=iterator.next();
					temp.addMsgFromParent(new ProcessMessage(this, temp, "Test"));
					msgSent++;
				}
			}
			while (this.getMsgFromChild().size()!= msgSent) {}
			ProcessMessage msg=selectMessageWithMinWt(this.getMsgFromChild(),this.adjList);
			this.getParent().addMsgFromChild(msg);
			this.msgFromChild.clear();
		}
	}//end of convergeCast

	/*selecting minimum weight edge*/
	public ProcessMessage selectMessageWithMinWt(HashSet<ProcessMessage> list,double[] adjLst){
		double minWt=Double.MAX_VALUE;
		int tempCompID=-1;
		SynchGHSProcess tempFrom=null,tempTo=null;
		Iterator<ProcessMessage> iterator=list.iterator();
		//select min weight message from acknowledgement received from child
		while(iterator.hasNext()){
			ProcessMessage temp=iterator.next();
			String[] token=temp.msg.split("#");
			double wt=Double.parseDouble(token[0]);
			if(wt<minWt){
				minWt=wt;
				tempCompID=Integer.parseInt(token[1]);
				tempFrom=temp.from;
				tempTo=temp.to;
			}
			else if (wt == minWt) {
				//Added by Chien-Ping: break the tie by processes' ID
				int currFrom = (int)temp.from.myID;
				int currTo = (int)temp.to.myID;
				int minFrom = (int)tempFrom.myID;
				int minTo = (int)tempTo.myID;
				
				if ((wt + currFrom + currTo) < (minWt + minFrom + minTo)) {
					minWt=wt;
					tempCompID=Integer.parseInt(token[1]);
					tempFrom=temp.from;
					tempTo=temp.to;
				}
			}
		}
		//comparing min wt process with adjacent process
		for(int i=0;i<adjLst.length;i++){
			SynchGHSProcess temp=this.getProcess(i);
			if(adjList[i]>0 && this.getLeader()!=temp.leader){
				if (adjList[i]<minWt) {
					minWt=adjList[i];
					tempCompID=i;
					tempFrom=this;
					tempTo=getProcess(i);
				}
				else if (adjList[i] == minWt) {
					//Added by Chien-Ping: break tie by processes' ID
					int currFrom = (int)temp.myID;
					int currTo = (int)getProcess(i).myID;
					int minFrom = (int)tempFrom.myID;
					int minTo = (int)tempTo.myID;
					
					if ((adjList[i] + currFrom + currTo) < (minWt + minFrom + minTo)) {
						minWt=adjList[i];
						tempCompID=i;
						tempFrom=this;
						tempTo=getProcess(i);
					}
				}
			}
		}
		String info=minWt+"#"+tempCompID+"#"+this.getMyID()+"#"+tempTo.getMyID();
		return (new ProcessMessage(this, tempTo, info));
	}

	public SynchGHSProcess getProcess(int cID){
		Iterator<SynchGHSProcess> iterator=Master.processes.iterator();
		while(iterator.hasNext()){
			SynchGHSProcess temp=iterator.next();
			if(temp.index==cID)
				return temp;
		}
		System.out.println("error in SynchGHSProcess.getProcess()");
		return null;
	}
	
	/*find process that belongs to given component*/
	public HashSet<SynchGHSProcess> findComponentProcess(int cID){
		HashSet<SynchGHSProcess> res=new HashSet<SynchGHSProcess>();
		Iterator<SynchGHSProcess> iterator=Master.processes.iterator();
		while(iterator.hasNext()){
			SynchGHSProcess temp=iterator.next();
			if(temp.getCompID()==cID)
				res.add(temp);
		}
		return res;
	}

	public void addMsgFromParent(ProcessMessage msg){
		this.getMsgFromParent().add(msg);
	}

	public void addMsgFromChild(ProcessMessage msg){
		this.getMsgFromChild().add(msg);
	}

	public void addMsgFromOtherComp(CompMessage msg){
		this.getMsgFromOtherComp().add(msg);
	}

	public void addMsgFromMaster(MasterMessage msg){
		try {
			this.msgFromMaster.put(msg);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		if(getParent()==null)
			return "ID: "+myID+" Parent: null Child: "+childInfo()+" Level: "+level+" CompID: "+compID+" Leader: "+leader.myID+" AmILeader: "+amILeader+" CompToJoin: "+compToJoin;
		else
			return "ID: "+myID+" Parent: "+parent.myID+" Child: "+childInfo()+" Level: "+level+" CompID: "+compID+" Leader: "+leader.myID+" AmILeader: "+amILeader+" CompToJoin: "+compToJoin;
	}

	public String childInfo(){
		StringBuffer res=new StringBuffer("[");
		Iterator<SynchGHSProcess> iterator=this.getChild().iterator();
		while(iterator.hasNext()){
			SynchGHSProcess temp=iterator.next();
			res.append(temp.getMyID()+" ");
		}
		res.append("]");
		return res.toString();
	}
}