public class ProcessMessage {
	SynchGHSProcess from,to;
	String msg;//format "test",  "min wt#compID#from Process myID#toProcess myID"
	public ProcessMessage(SynchGHSProcess from, SynchGHSProcess to, String msg) {
		this.from = from;
		this.to = to;
		this.msg = msg;
	}
	
	public String toString() {
		return msg;
	}
}