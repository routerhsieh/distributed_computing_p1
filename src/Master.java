import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
public class Master {
	int processCount;
	String fileName = "inputfile.txt";// input file name
	char[] processID;
	double[][] matrix;
	static HashSet<SynchGHSProcess> processes;
	static int phase = 0, step;
	static ArrayBlockingQueue<MasterMessage> msgFromComp;

	/* Parse the input and assign the value to instance variables */
	public void parse() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine().trim();
			String[] token = line.split("\\s+");
			this.processCount = Integer.parseInt(token[0]);
			msgFromComp = new ArrayBlockingQueue<MasterMessage>(
					processCount + 1);
			this.processID = new char[processCount];
			this.matrix = new double[processCount][processCount];
			line = reader.readLine().trim();
			token = line.split("\\s+");
			for (int i = 0; i < processCount; i++)
				processID[i] = token[i].charAt(0);
			for (int i = 0; i < processCount; i++) {
				line = reader.readLine().trim();
				token = line.split("\\s+");
				for (int j = 0; j < processCount; j++)
					matrix[i][j] = Double.parseDouble(token[j]);
			}
			processes = new HashSet<SynchGHSProcess>();
			for (int i = 0; i < processCount; i++) {
				processes.add(new SynchGHSProcess(processID[i], matrix[i], i));
			}
		} catch (Exception e) {
			System.out.println("Error in Master.parse(): " + e.getMessage());
		}
	}

	// steps to be followed in every phase
	private void initialize(){
		step = 1;
		int maxSteps = 3;
		Iterator<SynchGHSProcess> iterator = processes.iterator();
		while (true) {
			/* notify for second round */
			iterator = processes.iterator();
			while (iterator.hasNext()) {
				SynchGHSProcess temp = iterator.next();
				if(step==maxSteps)
					temp.addMsgFromMaster(new MasterMessage("done"));
				else if (step == 1)
					temp.addMsgFromMaster(new MasterMessage("Start phase#"+ phase + "#" + step));
				else
					temp.addMsgFromMaster(new MasterMessage("Start round#"+ step));
			}
			
			if(step==maxSteps)
				break;
			
			/* wait until all messages from components have been received */
			while (true) {
				if (msgFromComp.size() == processCount) {
					msgFromComp.clear();
					break;
				}
			}
			try {
				Thread.currentThread().sleep(1000);
			}
			catch (InterruptedException e) {
				System.out.println("error in sleep in Master class: "+e.getMessage());
			}
			/*System.out.println("After step: "+step);
			print();
			System.out.println();*/
			step++;
		}
	}

	void print() {
		Iterator<SynchGHSProcess> iterator = processes.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

	// executes the phases
	public void findMST() {
		/* start all the process */
		Iterator<SynchGHSProcess> iterator = processes.iterator();
		while (iterator.hasNext()) {
			iterator.next().start();// start the processes
		}
		while (processes.size() != 1) {// run for # phases(# till the number of
										// remaining component is equal to 1)
			if (phase == 0) {// currently running for only phase 0
				initialize();
				break;
			}
			phase++;// increment the current phase
		}
	}

	// main method. execution starts from this point
	public static void main(String[] args) {
		Master master = new Master();
		master.parse();
		/*System.out.println("Initially:");
		master.print();
		System.out.println();*/
		master.findMST();
	}
}