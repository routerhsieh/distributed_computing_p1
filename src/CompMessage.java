public class CompMessage{
	int from, to;
	String msg;//format "test",  "min wt#compID#from Process myID#toProcess myID"
	public CompMessage(int from, int to, String msg) {
		this.from = from;
		this.to = to;
		this.msg = msg;
	}
	
	public String toString() {
		return from+"-"+msg+"->"+to;
	}

}