public class MasterMessage {
	String msg;

	public MasterMessage(String msg) {
		this.msg = msg; //format: 'Start round#round no'    or 'Start phase#phase no#round no'
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String toString() {
		return msg;
	}
}